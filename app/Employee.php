<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'salary', 'position_id', 'company_id'
    ];

    public function position()
    {
        return $this->belongsTo('App\Position');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
