<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Employee extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'salary' => $this->salary,
            'position' => $this->position->name,
            'position_id' => $this->position_id,
            'company' => $this->company->name
        ];
    }
}
