<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PositionCollection;
use App\Position;

class PositionController extends Controller
{
    public function index()
    {
        return new PositionCollection(Position::all());
    }
}
