<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'comment' => 'required',
            'commented_company_id' => 'required|exists:companies,id',
            'commenting_company_id' => 'required|exists:companies,id'
        ]);

        $comment = new Comment();
        $comment->fill($request->all());
        $comment->save();

        return response()->json('Comment saved');
    }
}
