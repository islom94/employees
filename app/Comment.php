<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'commented_company_id', 'commenting_company_id',
    ];

    public function commenting_company()
    {
        return $this->hasOne('App\Company', 'id', 'commenting_company_id');
    }
}
