<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'comment'    => $faker->realText,
        'commented_company_id' => 1,
        'commenting_company_id' => rand(1, 25)
    ];
});
