require('./bootstrap')
import 'es6-promise/auto'
import axios from 'axios'
import auth from './auth'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.use(VueAxios, axios)
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`

import App from './components/App'
import Home from './components/Home'
import Companies from './components/Companies'
import Employees from './components/Employees'
import Settings from './components/Settings'
import Register from './components/Register'
import Login from './components/Login'
import Company from './components/Company'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                auth: undefined
            }
        },
        {
            path: '/companies',
            name: 'companies',
            component: Companies,
            meta: {
                auth: undefined
            }
        },
        {
            path: '/employees',
            name: 'employees',
            component: Employees,
            meta: {
                auth: undefined
            }
        },
        {
            path: '/settings',
            name: 'settings',
            component: Settings,
            meta: {
                auth: true
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/company/:id',
            name: 'view',
            component: Company,
            meta: {
                auth: undefined
            }
        }
    ],
});

Vue.router = router
Vue.use(VueAuth, auth)

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    vuetify: new Vuetify()
});